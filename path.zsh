# MacPorts
export PATH=/opt/local/bin:/opt/local/sbin:$PATH

# export PATH="/usr/local/opt/php@7.4/bin:$PATH"
# export PATH="/usr/local/opt/php@7.4/sbin:$PATH"
export PATH="$HOME/.composer/vendor/bin:$PATH"
export PATH="/usr/local/opt/mongodb-community@4.4/bin:$PATH"
source /usr/local/share/chruby/chruby.sh
chruby 2.7.4
