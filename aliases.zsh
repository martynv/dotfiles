# Navigation
alias h="cd ~"
alias d="cd ~/Desktop"
alias f="cd ~/Documents"
alias w="cd ~/workspaces"

# Laravel
alias pa="php artisan"


# DevOps
alias v="velero"


# Editors
alias vim="mvim -v"
alias vi="vim"

# PHP Versions
alias php81="brew unlink php && brew link php@8.1 && valet use php@8.1 --force"
alias php8="brew link php@8.0 && valet use php@8.0 --force"
alias php7="brew unlink php && brew link php@7.4 && valet use php@7.4 --force"

# Utilities
alias upt="uptime"
